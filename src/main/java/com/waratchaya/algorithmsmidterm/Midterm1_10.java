/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waratchaya.algorithmsmidterm;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Melon
 */
public class Midterm1_10 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        ArrayList<ArrayList<Integer>> collectSub = new ArrayList<>();
        ArrayList<Integer> inner = new ArrayList<Integer>();
        ArrayList<Integer> temp = new ArrayList<Integer>();
        ArrayList<ArrayList<Integer>> longestSub = new ArrayList<>();
        int checkmax = 0;
        System.out.print("How many number that you want to collect in array: ");
        int x = kb.nextInt();
        System.out.print("enter number: ");
        for (int i = 0; i < x; i++) {
            Integer num = kb.nextInt();
            inner.add(num);
        }
        for (int i = 0; i < inner.size(); i++) {
            if (i == inner.size() - 1) {
                if (temp.size() > checkmax) {
                    checkmax = temp.size();
                }
                collectSub.add(temp);
                break;
            }
            if (i == 0) {
                temp.add(inner.get(i));
            }
            for (int j = i + 1; j < i + 2; j++) {
                if (inner.get(i) < inner.get(j)) {
                    temp.add(inner.get(j));
                } else if (inner.get(i) >= inner.get(j)) {
                    if (temp.size() > checkmax) {
                        checkmax = temp.size();
                    }
                    collectSub.add(temp);
                    temp = new ArrayList<Integer>();
                    temp.add(inner.get(j));
                }
            }
        }
        int maxsize = 0;
        for (int i = 0; i < collectSub.size(); i++) {
            if (collectSub.get(i).size() == checkmax) {
                longestSub.add(collectSub.get(i));
            }
        }
        for (ArrayList lastprint : longestSub) {
            System.out.println(lastprint);
            maxsize = lastprint.size();
        }

        System.out.println("size is " + maxsize);

    }

}
