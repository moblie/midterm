/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waratchaya.algorithmsmidterm;

import java.util.Scanner;

/**
 *
 * @author Melon
 */
public class Midterm1_9 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String input_string = kb.next();
        String[] string_num = new String[input_string.length()];
        int[] number = new int[input_string.length()];
        for (int i = 0; i < input_string.length(); i++) {
            string_num[i] = input_string.substring(i, i + 1);
        }
        for (int i = 0; i < input_string.length(); i++) {
            if (string_num[i].equals("0")) {
                number[i] = 0;
            }
            if (string_num[i].equals("1")) {
                number[i] = 1;
            }
            if (string_num[i].equals("2")) {
                number[i] = 2;
            }
            if (string_num[i].equals("3")) {
                number[i] = 3;
            }
            if (string_num[i].equals("4")) {
                number[i] = 4;
            }
            if (string_num[i].equals("5")) {
                number[i] = 5;
            }
            if (string_num[i].equals("6")) {
                number[i] = 6;
            }
            if (string_num[i].equals("7")) {
                number[i] = 7;
            }
            if (string_num[i].equals("8")) {
                number[i] = 8;
            }
            if (string_num[i].equals("9")) {
                number[i] = 9;
            }

        }
        for (int i = 0; i < input_string.length(); i++) {
            System.out.print(number[i]);
        }
    }
}
